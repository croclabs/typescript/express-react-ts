import dotenv from "dotenv";
import express, { Express, Request, Response } from "express";
import cors from "cors";
import App from "./App";

import './controller/Controllers';

dotenv.config();

const app: Express = App.getApp();

app.use(express.json());
app.get('/', (req: Request, res: Response) => {
    res.send('Hello World From the Typescript Server!')
});

const port = process.env.PORT ?? 8000;

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
});
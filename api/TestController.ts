import { AbstractService, IController, IEndpoint, endpoint, get, controller } from "../controller/Controller";

class TestService extends AbstractService {
    test() {
        this.res.send({test: 'success'});
    }

    get() {
        this.res.send('Get success');
    }
}

export default class TestController implements IController {
    @endpoint('post', '/api/test', TestService)
    test(service: TestService): void {
        service.test();
    }

    @get('/api/get', TestService)
    get(service: TestService): void {
        service.get();
    }
}

@controller('/api', TestService)
export class TestController2 implements IController {
    getTest2(service: TestService) {
        service.get();
    }
}
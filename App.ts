import express, { Express, Request, Response } from "express";

export default class App {
    private static app: Express = express();

    static getApp(): Express {
        return App.app;
    }
}
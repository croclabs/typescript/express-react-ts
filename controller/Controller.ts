import { Request, Response } from "express";
import App from "../App";

class EndpointError extends Error {
    constructor(msg: string) {
        super(msg);
    }
}

class Controllers {
    static endpoints: IEndpoint[] = [];

    static contains(endpoint: IEndpoint) {
        if (Controllers.endpoints.filter(ep => ep.path === endpoint.path && ep.method === endpoint.method).length > 0) {
            throw new EndpointError(`The enpoint ${endpoint.method} ${endpoint.path} already exists. Make sure to define endpoints only once!`);
        }

        Controllers.endpoints.push(endpoint);
    }
}

interface IEndpoint {
    method: string,
    path: string
}

interface IController { }

interface IService {
    req: Request; 
    res: Response;
}

abstract class AbstractService implements IService {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req;
        this.res = res;
    }
}

class Service extends AbstractService { }

/* Decorators */

function addEndpoint(method: string, path: string, func: (req: Request, res: Response) => void) {
    Controllers.contains({method: method, path: path});

    console.log('Added Endpoint:', method.toUpperCase(), path);
    // @ts-ignore
    App.getApp()[method](path, func);
}

/**
 * Add to a class to add all functions as endpoints to the express server.
 * The endpoints use the name of the function, split by camel case, where the first
 * part is the method of the endpoint.
 * 
 * Example: getCoolEndpoint => GET cool/endpoint
 * 
 * @param prefix A prefix to use before the path of the endpoints
 * @param service A service class to be injected into the functions
 * @returns decorator function
 */
function controller(prefix:string = '', service: new (req: Request, res: Response) => IService = Service) {
    return (constructor: new () => IController, context: ClassDecoratorContext) => {
        let controller = new constructor();

        // @ts-ignore
        let endpoints = Object.getOwnPropertyNames(constructor.prototype).filter(p => typeof controller[p] === 'function' && p !== 'constructor');

        endpoints.forEach(ep => {
            let parts = ep.replace(/([a-z0-9])([A-Z])/g, '$1 $2').split(' ');
            let method = parts[0];
            parts.shift();
            parts = parts.map(p => p.toLowerCase());
            let path = `${prefix}${parts.length > 0 ? '/' + parts.join('/') : ''}`;

            // @ts-ignore
            addEndpoint(method, path, (req: Request, res: Response) => controller[ep](new service(req, res), req, res));
        });
    }
}

/**
 * Add to function to create an endpoint for this express server.
 * 
 * @param method The endpoint's method
 * @param path The endpoint's path
 * @param Service A service class to be injected into the function
 * @returns decorator function
 */
function endpoint(method: string, path: string, Service: new (req: Request, res: Response) => IService) {
    return (target: (service: any, req: Request, res: Response) => void, propertyKey: ClassMethodDecoratorContext<IController, (service: any, req: Request, res: Response) => void>) => {
        addEndpoint(method, path, (req: Request, res: Response) => target(new Service(req, res), req, res));
    }
}

/**
 * Add to function to create a GET endpoint for this express server.
 * 
 * @param path The endpoint's path
 * @param Service A service class to be injected into the function
 * @returns decorator function
 */
function get(path: string, Service: new (req: Request, res: Response) => IService) {
    return (target: (service: any, req: Request, res: Response) => void, propertyKey: ClassMethodDecoratorContext<IController, (service: any, req: Request, res: Response) => void>) => {
        addEndpoint('get', path, (req: Request, res: Response) => target(new Service(req, res), req, res));
    }
}

/**
 * Add to function to create a PUT endpoint for this express server.
 * 
 * @param path The endpoint's path
 * @param Service A service class to be injected into the function
 * @returns decorator function
 */
function put(path: string, Service: new (req: Request, res: Response) => IService) {
    return (target: (service: any, req: Request, res: Response) => void, propertyKey: ClassMethodDecoratorContext<IController, (service: any, req: Request, res: Response) => void>) => {
        addEndpoint('put', path, (req: Request, res: Response) => target(new Service(req, res), req, res));
    }
}


/**
 * Add to function to create a POST endpoint for this express server.
 * 
 * @param path The endpoint's path
 * @param Service A service class to be injected into the function
 * @returns decorator function
 */
function post(path: string, Service: new (req: Request, res: Response) => IService) {
    return (target: (service: any, req: Request, res: Response) => void, propertyKey: ClassMethodDecoratorContext<IController, (service: any, req: Request, res: Response) => void>) => {
        addEndpoint('post', path, (req: Request, res: Response) => target(new Service(req, res), req, res));
    }
}

/**
 * Add to function to create a DELETE endpoint for this express server.
 * 
 * @param path The endpoint's path
 * @param Service A service class to be injected into the function
 * @returns decorator function
 */
function del(path: string, Service: new (req: Request, res: Response) => IService) {
    return (target: (service: any, req: Request, res: Response) => void, propertyKey: ClassMethodDecoratorContext<IController, (service: any, req: Request, res: Response) => void>) => {
        addEndpoint('delete', path, (req: Request, res: Response) => target(new Service(req, res), req, res));
    }
}

export { IController, IEndpoint, IService, AbstractService, controller, endpoint, get, post, put, del };
